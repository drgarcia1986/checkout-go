package models

type ApiError struct {
	Error        error  `json:"-"`
	Code         int    `json:"-"`
	ErrorMessage string `json:"error_message"`
}
