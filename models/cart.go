package models

import (
	"encoding/json"
	"fmt"
	"github.com/drgarcia1986/checkout-go/helpers"
	"github.com/satori/go.uuid"
)

type Cart struct {
	Id       string `json:"id"`
	Customer string `json:"customer"`
}

func cacheKey(cart_id string) string {
	return fmt.Sprintf("cart-%s", cart_id)
}

func createCartId() string {
	return fmt.Sprint(uuid.NewV4())
}

func CartGet(cart_id string) (cart Cart, err *ApiError) {
	cart_bytes, _ := helpers.CheckoutCache.GetBytes(cacheKey(cart_id))
	if len(cart_bytes) == 0 {
		err = &ApiError{Code: 404, ErrorMessage: "cart not found"}
	} else {
		json.Unmarshal(cart_bytes, &cart)
	}
	return
}

func CartCreate(customer string) (cart Cart, err *ApiError) {
	cart_id := createCartId()
	cart = Cart{Id: cart_id, Customer: customer}
	cart_bytes, _ := json.Marshal(cart)
	helpers.CheckoutCache.SetBytes(cacheKey(cart_id), cart_bytes)
	return
}
