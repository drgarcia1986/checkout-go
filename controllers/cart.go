package controllers

import (
	"encoding/json"
	"github.com/astaxie/beego"
	"github.com/drgarcia1986/checkout-go/models"
)

type CartController struct {
	beego.Controller
}

func (c *CartController) Get() {
	cart_id := c.Ctx.Input.Params[":cart_id"]

	cart, err := models.CartGet(cart_id)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(err.Code)
		c.Data["json"] = err
	} else {
		c.Data["json"] = cart
	}
	c.ServeJson()
}

func (c *CartController) Post() {
	cart_serializer := models.Cart{}
	json.Unmarshal(c.Ctx.Input.RequestBody, &cart_serializer)

	cart, err := models.CartCreate(cart_serializer.Customer)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(err.Code)
		c.Data["json"] = err
	} else {
		c.Ctx.ResponseWriter.WriteHeader(201)
		c.Data["json"] = cart
	}
	c.ServeJson()
}
