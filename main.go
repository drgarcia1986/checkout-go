package main

import (
	"github.com/astaxie/beego"
	"github.com/drgarcia1986/checkout-go/controllers"
	"github.com/drgarcia1986/checkout-go/helpers"
)

func main() {
	//cache connection
	if err := helpers.CheckoutCache.Connect("127.0.0.1", 6379); err != nil {
		panic(err)
	}
	beego.RESTRouter("/cart/?:cart_id", &controllers.CartController{})
	beego.Run()
}
