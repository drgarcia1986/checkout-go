package helpers

import (
	"fmt"
	"github.com/garyburd/redigo/redis"
)

type cache struct {
	conn redis.Conn
}

var CheckoutCache cache

func (c *cache) Connect(host string, port int) error {
	var err error
	c.conn, err = redis.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
	return err
}

func (c cache) Get(key string) (string, error) {
	return redis.String(c.conn.Do("GET", key))
}

func (c cache) GetBytes(key string) ([]byte, error) {
	return redis.Bytes(c.conn.Do("GET", key))
}

func (c cache) Set(key, value string) {
	c.conn.Do("SET", key, value)
}

func (c cache) SetBytes(key string, value []byte) {
	c.conn.Do("SET", key, value)
}
